## Bus 527 from Sollentuna station
This skill tells if there is time to go to the bus, if you want to take bus 527 from Sollentuna station. 

## Description 
This should be seen as a proof of concept, dedicated for my brother. In a near future, I might build a skill for the SL-trafic in general.

## Examples 
* "Do I have to go to the bus now?"

## Credits 
Jon Johnson, Prograde
