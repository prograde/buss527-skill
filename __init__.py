# Copyright 2019 Jon Johnson, Prograde
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from adapt.intent import IntentBuilder
from mycroft.skills.core import MycroftSkill, intent_handler
from mycroft.util.log import LOG
import urllib.request
import json
import dateutil.parser
import datetime

class GotobusSkill(MycroftSkill):

    def __init__(self):
        super(GotobusSkill, self).__init__(name="GotobusSkill")
        self.count = 0

    @intent_handler(IntentBuilder("").require("Gotobus"))
    def handle_gotobus_intent(self, message):
        minutesToNextDeparture = getMinutes()
        if minutesToNextDeparture < 5:
            self.speak_dialog("dont.go", {"minutes": minutesToNextDeparture})
        elif minutesToNextDeparture < 8:
           self.speak_dialog("go.now", {"minutes": minutesToNextDeparture})
        else:
            self.speak_dialog("go.soon", {"minutes": minutesToNextDeparture})

def create_skill():
    return GotobusSkill()

def getMinutes():
  url = "http://api.sl.se/api2/realtimedeparturesV4.json?key=95a3e2874bbb4e9caa91a9f8b0b2eacb&siteid=9506&timewindow=20"

  response = urllib.request.urlopen(url).read()
  buses = json.loads(response)["ResponseData"]["Buses"]
  buses527 = filter(lambda bus: bus["LineNumber"] == "527", buses)
  buses527 = sorted(buses527, key=lambda bus: bus["ExpectedDateTime"])

  if len(buses527) < 1:
    return -1

  nextDeparture = dateutil.parser.parse(buses527[0]["ExpectedDateTime"])
  duration = nextDeparture - datetime.datetime.now()
  return int(duration.total_seconds()/60)
